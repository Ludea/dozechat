#ifndef API_H
#define API_H

#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/strand.hpp>
#include <boost/asio/bind_executor.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "network.h"
#include "http_router.h"
#include "logger.h"
#include <iostream>
#include <algorithm>
#include <thread>
#include <string>
//#include <jwt/jwt.hpp>

using tcp = boost::asio::ip::tcp;
namespace http = boost::beast::http;
//using namespace jwt::params;

class api : public std::enable_shared_from_this<api>
{
  struct send_lambda
    {
        api& self;

        explicit
        send_lambda(api& self)
            : self(self)
        {
        }

        template<bool isRequest, class Body, class Fields>
        void
        operator()(http::message<isRequest, Body, Fields>&& msg) const
        {
            // The lifetime of the message has to extend
            // for the duration of the async operation so
            // we use a shared_ptr to manage it.
            auto sp = std::make_shared<
                http::message<isRequest, Body, Fields>>(std::move(msg));

            // Store a type-erased version of the shared
            // pointer in the class to keep it alive.
            self.res = sp;

            // Write the response
            http::async_write(
                self.stream,
                *sp,
                boost::beast::bind_front_handler(
                        &api::on_write,
                        self.shared_from_this(),
                        sp->need_eof()));
        }
  };
//    tcp::socket socket;
    boost::beast::tcp_stream stream;
    boost::beast::flat_buffer buffer;
    std::shared_ptr<std::string const> doc_root;
    http::request<http::string_body> req;
    std::shared_ptr<void> res;
    send_lambda lambda;

public:
  api(tcp::socket&& socket, std::shared_ptr<std::string const> const& doc_root);
  void run();

private:
   void verify_token(std::string token);
   void do_read();
   void on_read(boost::beast::error_code ec, std::size_t bytes_transferred);
   void on_write(bool close, boost::beast::error_code ec, std::size_t bytes_transferred);
   void do_close();
   std::error_code ec;
   std::string key = "7D948952ECA04ED47803593C34C99335BE230CF296D0D3CC6ACF5E2CCEAB8F5B";
   std::string path_cat(boost::beast::string_view base, boost::beast::string_view path);
   boost::beast::string_view mime_type(boost::beast::string_view path);
   network *net;
   route::Route router;
   logger log;
template<class Body, class Allocator, class Send>
void handle_request(boost::beast::string_view doc_root, http::request<Body, http::basic_fields<Allocator>>&& req, Send&& send)
{
    // Returns a bad request response
    auto const bad_request =
    [&req](boost::beast::string_view why)
    {
        http::response<http::string_body> res{http::status::bad_request, req.version()};
        res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
        res.set(http::field::content_type, "text/html");
        res.keep_alive(req.keep_alive());
        res.body() = std::string(why);
        res.prepare_payload();
        return res;
    };

    // Returns a not found response
    auto const not_found =
    [&req](boost::beast::string_view target)
    {
        http::response<http::string_body> res{http::status::not_found, req.version()};
        res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
        res.set(http::field::content_type, "text/html");
        res.keep_alive(req.keep_alive());
        res.body() = "The resource '" + std::string(target) + "' was not found.";
        res.prepare_payload();
        return res;
    };

    auto const server_error =
    [&req](boost::beast::string_view what)
    {
        http::response<http::string_body> res{http::status::internal_server_error, req.version()};
        res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
        res.set(http::field::content_type, "text/html");
        res.keep_alive(req.keep_alive());
        res.body() = "An error occurred: '" + std::string(what) + "'";
        res.prepare_payload();
        return res;
    };

      // Handle an unknown error
    if(ec)
        return send(server_error(ec.message()));

    // Make sure we can handle the method
   // std::string path = path_cat(doc_root, req.target());
    //auto match = router.set(std::string(req.target()));
    std::string path = path_cat(doc_root, req.target());
    boost::beast::error_code ec;
    http::file_body::value_type body;
    body.open(path.c_str(), boost::beast::file_mode::scan, ec);
    auto const size = body.size();
   switch(req.method())
   {
   case http::verb::get:
	/*if(match.test("/user/:id/network"))
	{
	//	if(req.base()[http::field::authorization] == "test")
		verify_token(std::string(req.base()[http::field::authorization]));
		if(net->getUserNetworks(std::stoi(match.get("id"))) == "true")
		{
                	std::string path = path_cat(doc_root, req.target());
        		http::response<http::string_body> res{http::status::ok, req.version()};
        		res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
        		res.set(http::field::content_type, mime_type(path));
        		res.keep_alive(req.keep_alive());
        		res.set(http::field::access_control_allow_origin, "*");
        		res.set(boost::beast::http::field::access_control_allow_methods, "POST, OPTIONS");
        		res.set(boost::beast::http::field::access_control_allow_headers, "X-Requested-With, Content-Type, Origin, Accept");
        		res.set(boost::beast::http::field::access_control_allow_credentials, "true");
        		res.body() = "true";
        		return send(std::move(res));
		}
		else
		{
                        std::string path = path_cat(doc_root, req.target());
                        http::response<http::string_body> res{http::status::ok, req.version()};
                        res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
                        res.set(http::field::content_type, mime_type(path));
                        res.keep_alive(req.keep_alive());
                        res.set(http::field::access_control_allow_origin, "*");
                        res.set(boost::beast::http::field::access_control_allow_methods, "POST, OPTIONS");
                        res.set(boost::beast::http::field::access_control_allow_headers, "X-Requested-With, Content-Type, Origin, Accept");
                        res.set(boost::beast::http::field::access_control_allow_credentials, "true");
                        res.body() = "true";
                        return send(std::move(res));
		}
	}*/
	if( req.target() == "/health")
        {
		log.info("/health");
		std::string str ("OK");
    		http::response<http::string_body> res{http::status::ok, req.version()};
    		res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
    		res.set(http::field::content_type, "text/plain");
    		res.content_length(str.size());
    		res.keep_alive(req.keep_alive());
    		res.body() = str;
    		return send(std::move(res));
        }
	else
                return send(not_found(req.target()));
	break;
	case http::verb::post:
	if(req.target() == "/network/create")
	{
		std::map<std::string, std::string> params;
    		std::string keyval, key, value, networkName, networkJson;
		std::istringstream iss(req.body());
		while(std::getline(iss, keyval, '&'))
    		{
			std::istringstream iss(keyval);
			if(std::getline(std::getline(iss, key, '='), value))
			{
				if(key == "name")
					networkName = value ;
			}
			else
				networkJson = keyval ;

		}
		net->save(networkName, networkJson);
		std::string path = path_cat(doc_root, req.target());
                http::response<http::string_body> res{http::status::ok, req.version()};
                res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
                res.set(http::field::content_type, mime_type(path));
                res.keep_alive(req.keep_alive());
                res.set(http::field::access_control_allow_origin, "*");
                res.set(boost::beast::http::field::access_control_allow_methods, "POST, OPTIONS");
                res.set(boost::beast::http::field::access_control_allow_credentials, "true");
                res.body() = "true";
                return send(std::move(res));
	}
	else
		return send(not_found(req.target()));
	break;
	default:
        return send(bad_request("Unknown HTTP-method"));
	}

    // Request path must be absolute and not contain "..".
    if( req.target().empty() ||
        req.target()[0] != '/' ||
        req.target().find("..") != boost::beast::string_view::npos)
        return send(bad_request("Illegal request-target"));
}

void response(std::string response)
{
//	std::string path = path_cat(doc_root, req.target());
	http::response<http::string_body> res{http::status::ok, req.version()};
        res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
//        res.set(http::field::content_type, mime_type(path));
        res.keep_alive(req.keep_alive());
        res.set(http::field::access_control_allow_origin, "*");
        res.set(boost::beast::http::field::access_control_allow_methods, "POST, OPTIONS");
        res.set(boost::beast::http::field::access_control_allow_headers, "X-Requested-With, Content-Type, Origin, Accept");
	res.body() = response;
//	return send(std::move(res));
}

};

class listener //: public std::enable_shared_from_this<listener>
{
    tcp::acceptor acceptor;
    boost::asio::io_context& m_ioc;
    std::shared_ptr<std::string const> doc_root;

public:
    listener(boost::asio::io_context& ioc, tcp::endpoint endpoint, std::shared_ptr<std::string const> const& doc_root);
    void do_accept();
    void run();
    void on_accept(boost::beast::error_code ec, tcp::socket socket);
private:
    logger log;
};
#endif //API_H
