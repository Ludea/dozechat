#ifndef STORAGE_H
#define STORAGE_H

#include <mysql.h>
#include <iostream>

class storage
{
public:
   storage();
   void connection_db();
   void insert_channel_data();
   void insertNetwork(std::string networkName, std::string networkJson);
   void saveUserintoNetwork(int id_user, std::string listNetwork);
   std::string getData(int id_user);

private:
   MYSQL *database;
   const char *user, *password, *db, *socket_path, *host, *table_request; //, *getEntry;
   unsigned int port;
   const char *error_message;
   void create_database_table();
   void close();
   std::string db_request ;
};

#endif //STORAGE_H
