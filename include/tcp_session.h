#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>

#include "message.h"
#include "chatroom.h"

class tcp_session
  : public chat_participant,
    public std::enable_shared_from_this<tcp_session>
{
public:
  tcp_session(tcp::socket socket, chatroom& room);
  void start();
  void deliver(const message& msg);

private:
  void do_read_header();
  void do_read_body();
  void do_write();

  tcp::socket socket;
  chatroom& room;
  message read_msg;
  chat_message_queue write_msgs;
};
