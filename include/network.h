#ifndef NETWORK_H
#define NETWORK_H

#include "storage.h"
#include <iostream>

class network
{
  public:
    network();
    void save(std::string name, std::string channel_users);
    void souscribeNetwork(int id_user, std::string listNetwork);
    std::string getUserNetworks(int id_user);
  private:
   storage *store;
};

#endif //network_h
