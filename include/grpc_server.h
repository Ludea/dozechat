#include <grpcpp/grpcpp.h>
#include "grpc_server.grpc.pb.h"

//#include "json_parser.h"
#include "encoder.h"
#include "logger.h"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;
using grpc::ServerCompletionQueue;
using grpc::ServerAsyncResponseWriter;
using doze::SDK;
using doze::Empty;
using doze::serverStatus;
using doze::Update;
using doze::localVersion;

class Call
{
    public:
        virtual void Proceed() = 0;
};

class CallCheckUpdate final: public Call
{
public:
    CallCheckUpdate(SDK::AsyncService* service, ServerCompletionQueue* cq);
    void Proceed();

private:
    SDK::AsyncService* service;
    ServerCompletionQueue* cq;
    ServerContext ctx;
    localVersion request;
    Update reply;
    ServerAsyncResponseWriter<Update> responder;

    // Let's implement a tiny state machine with the following states.
    enum CallStatus{ CREATE, PROCESS, FINISH };
    CallStatus status;  // The current serving state.
//    json_parser JsonParser;
    encoder Encoder;
};

class grpc_server final
{
public:
	~grpc_server();
	void run();
private:
    void HandleRpcs() ;
    std::unique_ptr<ServerCompletionQueue> cq;
    SDK::AsyncService service;
    std::unique_ptr<Server> server;
    logger log;
};
