#include "storage.h"
#include <iostream>

storage::storage()
{
	connection_db();
	if (mysql_select_db(database, "Doze"))
        {
            create_database_table();
        }
    	else
    	{
        	error_message = mysql_error(database);
        	std::cout<<error_message<<std::endl;
    	}
	close();
}

void storage::connection_db()
{
	host = "localhost";
	port = 3306;
	user = "root";
	password = "coucou";
	db = "";
	socket_path = "/run/mysqld/mysqld.sock";
    database = mysql_init(NULL);
    if (database != mysql_real_connect(database, host, user, password, db, port, socket_path, CLIENT_IGNORE_SPACE ))
    {
       error_message = mysql_error(database);
       std::cout<<error_message<<std::endl;
    }
}

//TODO : create all tables
void storage::create_database_table()
{
    std::cout<<"table creating..."<<std::endl;
    db_request = "CREATE DATABASE Doze";
    //ToDo do mysql_real_query
    if(mysql_query(database, db_request.c_str()))
    {
        error_message = mysql_error(database);
        std::cout<<error_message<<std::endl;
        std::cout<<"database not created..."<<std::endl;
    }
    else
    {
	mysql_select_db(database, "Doze");
        table_request = "CREATE TABLE users (id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT, login VARCHAR(255), password VARCHAR(255), email VARCHAR(255));";
        if(mysql_query(database, table_request))
        {
            error_message = mysql_error(database);
            std::cout<<error_message<<std::endl;
        }
	table_request = "CREATE TABLE network ( id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT, name VARCHAR(255), channel VARCHAR(255));";
        if(mysql_query(database, table_request))
        {
            error_message = mysql_error(database);
            std::cout<<error_message<<std::endl;
	}
	table_request = "CREATE TABLE listNetworkperUser ( id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT, id_user INT(11), listNetwork VARCHAR(255), FOREIGN KEY(id_user) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE);";
	if(mysql_query(database, table_request))
        {
            error_message = mysql_error(database);
            std::cout<<error_message<<std::endl;
        }
    }
}

void storage::insert_channel_data()
{
  /* add_entry = "INSERT INTO Clients VALUES ('', 'test1', 'test3', 'test4') ";
   if(mysql_query(database, addEntry))
        {
            error_message = mysql_error(database);
            std::cout<<error_message<<std::endl;
        }*/
}

std::string storage::getData(int id_user)
{
  std::string list;
  connection_db();
  mysql_select_db(database, "Doze");
  std::string getEntry;
  getEntry = "SELECT listNetwork FROM listNetworkbyUser WHERE id_user = '%d'", id_user;
  if(mysql_query(database, getEntry.c_str()))
  {
      error_message = mysql_error(database);
      std::cout<<error_message<<std::endl;
  }
  MYSQL_RES *result = mysql_store_result(database);
  if (result)
  {
    int num_fields = mysql_num_fields(result);
    MYSQL_ROW row;
    while ((row = mysql_fetch_row(result)))
    {
	for(int i = 0; i < num_fields; i++)
	{
		list = row[i];
		return list;
	}
    }
    mysql_free_result(result);
  }
  else
	list = "false";
  close();
  return list;
}

void storage::insertNetwork(std::string networkName, std::string networkJson)
{
  connection_db();
  mysql_select_db(database, "Doze");
  db_request = "INSERT INTO network (name, channel) VALUES ('" +networkName+ "', '" +networkJson+ "');";
  if(mysql_query(database, db_request.c_str()))
  {
     error_message = mysql_error(database);
     std::cout<<error_message<<std::endl;
  }
  close();
}

void storage::saveUserintoNetwork(int id_user, std::string listNetwork)
{
  connection_db();
  db_request = "INSERT INTO listNetworkperUser (id_user, listNetwork) VALUES (id_user, listBetwork);  ";
  if(mysql_query(database, db_request.c_str()))
  {
	error_message = mysql_error(database);
	std::cout<<error_message<<std::endl;
  }
  close();
}

void storage::close()
{
  mysql_close(database);
}
