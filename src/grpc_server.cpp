#include "grpc_server.h"

grpc_server::~grpc_server()
{
    server->Shutdown();
    // Always shutdown the completion queue after the server.
    cq->Shutdown();
}

void grpc_server::run()
{
	std::string server_address("0.0.0.0:50051");
	ServerBuilder builder;
  	// Listen on the given address without any authentication mechanism.
  	builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  	// Register "service" as the instance through which we'll communicate with
  	// clients. In this case it corresponds to an *synchronous* service.
  	builder.RegisterService(&service);
	cq = builder.AddCompletionQueue();
   	server = builder.BuildAndStart();
  	// Finally assemble the server.
	log.info("gRPC server started !");

  	HandleRpcs();
}

void grpc_server::HandleRpcs()
{
        new CallCheckUpdate(&service, cq.get());
    	void* tag;  // uniquely identifies a request.
    	bool ok;
    	while (true)
	{
      		// Block waiting to read the next event from the completion queue. The
      		// event is uniquely identified by its tag, which in this case is the
      		// memory address of a CallData instance.
      		// The return value of Next should always be checked. This return value
      		// tells us whether there is any kind of event or cq_ is shutting down.
      		GPR_ASSERT(cq->Next(&tag, &ok));
      		GPR_ASSERT(ok);
      		static_cast<CallCheckUpdate*>(tag)->Proceed();
    	}
}

CallCheckUpdate::CallCheckUpdate(SDK::AsyncService* service, ServerCompletionQueue* cq) : service(service), cq(cq), responder(&ctx), status(CREATE)
{
    Proceed();
}

void CallCheckUpdate::Proceed()
{
      	if (status == CREATE)
	{
        	// Make this instance progress to the PROCESS state.
        	status = PROCESS;

        	// As part of the initial CREATE state, we *request* that the system
        	// start processing SayHello requests. In this request, "this" acts are
        	// the tag uniquely identifying the request (so that different CallData
        	// instances can serve different requests concurrently), in this case
        	// the memory address of this CallData instance.
        	service->RequestcheckUpdate(&ctx, &request, &responder, cq, cq, this);
    }
	else
	if (status == PROCESS)
	{
        	// Spawn a new CallData instance to serve new clients while we process
        	// the one for this CallData. The instance will deallocate itself as
        	// part of its FINISH state.
            new CallCheckUpdate(service, cq);

        	// The actual processing.
            if (true) // > request.version())
            {
                 reply.set_availableupdate(true);
                 //reply.set_version(1xJsonParser.GetLatestVersion());
                //check if patch already exist
                //if not, create patch
//                Encoder.createPatch();
            }

            else
                reply.set_availableupdate(false);
        	// And we are done! Let the gRPC runtime know we've finished, using the
        	// memory address of this instance as the uniquely identifying tag for
        	// the event.
        	status = FINISH;
        	responder.Finish(reply, Status::OK, this);
      	}
	else
	{
        	GPR_ASSERT(status == FINISH);
        	// Once in the FINISH state, deallocate ourselves (CallData).
        	delete this;
      	}
    }
